Arduino Uno
+---------------------------+
| GND   VCC   D2   D3   D4  |
|                           |
| GND ----> 4-Digit Display |
| 5V  ----> 4-Digit Display |
| D2  ----> 4-Digit Display |
| D3  ----> 4-Digit Display |
|                           |
| D4  ----> 4x4 Button Mat  |
| D5  ----> 4x4 Button Mat  |
| D6  ----> 4x4 Button Mat  |
| D7  ----> 4x4 Button Mat  |
| D8  ----> 4x4 Button Mat  |
| D9  ----> 4x4 Button Mat  |
| D10 ----> 4x4 Button Mat  |
| D11 ----> 4x4 Button Mat  |
+---------------------------+

4-Digit Display
+---------------------+
| GND ----> Arduino   |
| VCC ----> Arduino   |
| DIO ----> Arduino D2|
| CLK ----> Arduino D3|
+---------------------+

4x4 Button Matrix
+----------------------------+
| ROW1 ----> Arduino D4      |
| ROW2 ----> Arduino D5      |
| ROW3 ----> Arduino D6      |
| ROW4 ----> Arduino D7      |
| COL1 ----> Arduino D8      |
| COL2 ----> Arduino D9      |
| COL3 ----> Arduino D10     |
| COL4 ----> Arduino D11     |
+----------------------------+
