# Automatic Wi-Fi Connection Configuration File

## Method I: With Raspberry Pi Imager

To configure the automatic connection file to a specific network, you can use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/) software. Once the SD card is inserted to configure the OS, an option will appear after clicking "Next." Here, you will find modifications you can apply if desired, including enabling SSH and especially the ability to enter the network SSID and associated password.

You then save your changes and start writing to the SD card. Afterward, insert the SD card into the Raspberry Pi.

## Method II: By Creating the File Manually

For this second method, after configuring the SD card without applying the network connection modifications, go to the root of the SD card, in the "bootfs" directory. Here, insert a file named "wpa_supplicant.conf."

In this file, enter the following lines:

```plaintext
country=FR
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="NetworkName"
    psk="Password"
}
```


Save the file, then insert the SD card into the Raspberry Pi.

These two methods allow Raspberry Pi devices to automatically connect to a predefined network upon startup.
