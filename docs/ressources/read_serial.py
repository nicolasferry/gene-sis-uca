import serial

# Define the serial port and communication speed (baud rate)
ser = serial.Serial('/dev/ttyACM0', 9600)  # Ensure this is the correct port

while True:
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').rstrip()
        print(line)
