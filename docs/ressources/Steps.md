# Software Deployment Steps

### Note: For deployment, the components must be on the same network!

In most scenarios, I use Node-RED to test my deployments. Node-RED is a low-code development tool based on flow-based programming. It uses the concept of data flow programming. Instead of typing programming commands in a text editor, it uses a visual flow editor as a graphical environment. To create a network of small pre-built code blocks, called "nodes."

## Step 1: Deploying Node-RED on a Cloud Resource

This scenario involves a simple deployment where an application is hosted directly on a Cloud resource. The application runs exclusively in the Cloud, without interaction with Edge components or local IoT devices. I deployed an empty Node-RED on my machine with Docker to familiarize myself with the tool and to check if the deployment works well.

## Step 2: Deploying Node-RED with an Application

In this scenario, I perform the same steps as in Step 1, but I deploy a Node-RED that contains an application, a flow.

## Step 3: Communication between Two Machines, Cloud and Edge

This step involves deploying a component of an application from the Cloud to the Edge. This creates a two-tier architecture where the Raspberry Pi acts as the Edge, and my laptop acts as the Cloud. The configuration is as follows: my laptop has a Node-RED with a flow that allows exchanging messages with a nearby Raspberry Pi, which also has a Node-RED performing the same procedure. Thus, there is an exchange of information between the Raspberry Pi and my machine with the two Node-RED instances. Both Node-RED instances are hosted under Docker as well.

## Step 4: Adding an Arduino Application to the Edge-Cloud Infrastructure

In this step, a new application is added to the existing infrastructure from step 3. This application runs on an Arduino, which is connected to the Edge component, the Raspberry Pi. The Arduino generates data via a running timer and sends the information to the Edge component. A coded remote control is then provided to start the timer, pause it, restart it, or reset it. The timer can then transmit data to the Cloud. This integration enhances the Edge infrastructure's ability to interact with specific IoT devices and provide a fast local response. Here, we have an IoT - Edge - Cloud infrastructure.

## Step 5: Extending the Previous Infrastructure with Multiple Raspberry Pi Devices, IoT - Edge - Cloud

This step extends the infrastructure from step 4 by adding multiple Raspberry Pi devices to enhance the Edge level. Each Raspberry Pi can host specific applications or services, collect data from different IoT devices, and interact with the Cloud component. The goal here is to deploy on the various Raspberry Pi devices, with the Arduino still connected to one of them.
